package com.qweex.utils.examples;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.qweex.utils.R;

public class PathPreferenceExample extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.page_preference);
    }
}
