package com.qweex.utils.examples;

import android.app.Activity;
import android.os.Bundle;
import com.qweex.utils.XBMCStyleListViewMenu;

import java.util.ArrayList;

public class XBMCExample extends Activity {
    XBMCStyleListViewMenu listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listView = new XBMCStyleListViewMenu(this);
        ArrayList<String> items = new ArrayList<String>();
        items.add("herpaderp");
        items.add("butts");
        items.add("things");
        items.add("idunno");
        listView.setData(items);
        listView.setBackgroundColor(getResources().getColor(android.R.color.white));
        listView.setCacheColorHint(getResources().getColor(android.R.color.white));
        setContentView(listView);
    }
}
