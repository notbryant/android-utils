# Qweex Android utils

This is a repo where I am dumping all the little things I've picked up for Android that might be useful for general use.

Please feel free to expand on any of them.
Unless otherwise specified, everything in here is released under the WTFPL.

There is also a small activity that demos them, if you are curious.

## DirectoryChooserDialog ##
A simple directory for choosing a dialog.

Lets you set whether or not a "New Folder" button will be offered.

## PathPreference ##
DirectoryChooserDialog implemented in a Preference.

## MultiFilePickerDialog ##
A dialog for the user to select multiple files in a single folder.

Lets you specify a start folder.

## ImgTxtButton ##
A widget that lets have an image and text on the same button and NOT have it look like shit.

Lets you set the drawable, text, and orientation (either vertical or horizontal).

## Crypt ##
Some dirt simple functions to use when encrypting data.

Get the Key, encrypt. Get the Key, decrypt. Simple.

Offers the ability to customize things like what algorithm, the key size, how many iterations to perform, etc.
(Note: Access Crypt.SALT to get the salt!)

## NumberPicker ##
A number picker with support for things like prefix and suffix and custom increment.

## XBMCStyleListMenu ##
A listview that attempts to mimic the style like on a HTPC. Kind of sucks to be honest.

## ArrayListWithMaximum ##
An array list with a maximum size. What do you want from me?

## IsTabletDevice ##
A fairly accurate function to determine if the device is a tablet.